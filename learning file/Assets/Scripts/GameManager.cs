using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{
    //can get from anywhere
    public static GameManager instance;
    public AudioSource Swing;
    public AudioSource Stab;
    public AudioSource Coin;


    //make sure we only have one instance -- the first game manager it finds in the scene
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
        /*SceneManager.sceneLoaded += SaveState;
        SceneManager.sceneLoaded += LoadState;*/

        
    }

    // Resources
    public List<Sprite> playerSprites;
    public List<Sprite> weaponSprites;
    public List<int> weaponPrices;

    // References (to scripts): TODO = add weapons
    public Player player;

    // Logic
    public int money;
    public int score;



    //public void gameOver();

    

    public void PlaySwing()
    {
        Swing.Play();
    }

    public void PlayStab()
    {
        Stab.Play();
    }

    public void PlayCoin()
    {
        Coin.Play();
    }





}
