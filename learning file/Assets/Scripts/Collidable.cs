using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//auto adds a boxCollider
//[RequireComponent(typeof(BoxCollider2D))]
public class Collidable : MonoBehaviour
{
    public ContactFilter2D filter;
    private BoxCollider2D boxCollider;

    //array of what you collide with
    private Collider2D[] hits = new Collider2D[10];

    protected virtual void Start()
    {
        boxCollider = GetComponent<BoxCollider2D>();
    }

    protected virtual void Update()
    {
        //Collision work

        //get a list of all the colliders that overlap this collider
        boxCollider.OverlapCollider(filter, hits);

        //only continuing to OnCollide if i is a valid collider
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i] == null)
                continue;

            //function so objects can decide what they want to do when there's a collision
            OnCollide(hits[i]);

            //clean up array
            hits[i] = null;
        }
    }

    //inheritance
    protected virtual void OnCollide(Collider2D coll)
    {
        //Debug.Log("OnCollide not implemented in "+ this.name);
    }
}
