using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponEnemy : Collidable
{
    //Damage structure
    public int damagePoint = 1;
    public float pushForce = 2.0f;

    // Upgrade
    public int weaponLevel = 0;
    private SpriteRenderer spriteRenderer;


    //Attack
    private float cooldown;
    private float lastSwing;
    private Animator anim;


    protected override void Start()
    {
        base.Start();

        
        //so we can switch out weapon sprite later
        spriteRenderer = GetComponent<SpriteRenderer>();

        anim = GetComponent<Animator>();
    }


    protected override void Update()
    {
        base.Update();


        cooldown = Random.Range(1f, 4f);
        //TODO: only attack when close
        if (Time.time - lastSwing > cooldown)
        {
            lastSwing = Time.time;
            Attack();
        }
    }


    // what are we hitting
    protected override void OnCollide(Collider2D coll)
    {
        if (coll.name == "Player")
        {

            //sending a new damage object to hitted enemy
            Damage dmg = new Damage
            {
                damageAmount = damagePoint,
                origin = transform.position, //origin = enemy
                pushForce = pushForce
            };

            coll.SendMessage("ReceiveDamage", dmg);
        }


    }

    private void Attack()
    {
        anim.SetTrigger("Stab");
    }
}
